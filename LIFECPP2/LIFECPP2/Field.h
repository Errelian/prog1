#pragma once
#pragma once
class Cell
{
private:
    bool life;
    int neigbourcount;
public:
    Cell()
    {
        life = false;
        neigbourcount = 0;
    }
    bool getlife(){return life;}
    void setlife(bool x){life = x;}
    int getneighbour(){return neigbourcount;}
    void setneighbourcount(int x){neigbourcount = x;}
    void incNeigh(){neigbourcount++;}

};
class Field
{
public:
    Cell field[12][12];
    Field()
        {
            for (int i = 1; i<10;i++)
            {
                for(int j = 1; j < 10; j++)
                {
                    if(j + i == 7)
                    {
                        field[i][j].setlife(true);
                    }
                }
            }
        }
    void nextgen()
               {
                   for(int i = 1; i <10;i++)
                   {
                       for(int j =1; j <10; j++)
                       {
                           for (int k =i-1; k<i+2; k++)
                           {
                               for (int l = j-1; l <j+2;l++)
                               {
                                   if(field[k][l].getlife() == true)
                                   {
                                       field[i][j].incNeigh();
                                   }
                               }
                           }
                       }
           }
           for (int i =1; i <10; i++)
           {
               for (int  j=1; j<10;j++)
               {
                   if(field[i][j].getneighbour() > 3 || field[i][j].getneighbour() < 2)
                   {
                       field[i][j].setlife(false);
                   }
                   else
                   {
                       if (field[i][j].getneighbour() == 3)
                       {
                           field[i][j].setlife(true);
                       }
                   }
                   field[i][j].setneighbourcount(0);
               }
           }
       }
};

