#include <iostream>

template <typename T>
class BinTree
{
protected:
    class Node
    {
    private:
        T value;
        Node *left;
        Node *right;        //változók
        int count{ 0 };

        Node(const Node &) = delete;
        Node &operator=(const Node &) = delete;  //másoló és mozgató operátorok
        Node(Node &&) = delete;
        Node &operator=(Node &&) = delete;
    public:
        explicit Node(T value) { //paraméterizált konstruktor, most CPP
            this->value = value;
            this->left = nullptr;
            this->right = nullptr;
        }
        T getValue()    //value értéket returnoli
        {
            return value;
        }
        Node *leftChild()      //leftet returnol duh
        {
            return left;
        }
        Node *rightChild()      //rightot returnol
        {
            return right;
        }
        void leftChild(Node *node)      //duh
        {
            left = node;
        }
        void rightChild(Node *node)
        {
            right = node;
        }
        int getCount()
        {
            return count;
        }
        void incCount()     //OO alapja
        {
            count++;
        }
    };
    Node *root;         //Node pointer gyökér
    Node *treep;        // A fán belöli nodera mutató pointer
    int depth{ 0 };     // Fának a mélysége


private:
    //konstruktorok és operátor overrideok
    Node * cp(Node *node, Node *treep)
    {
        Node * newNode = nullptr;
        if(node)
        {
            newNode = new Node(node->getValue());

            newNode->leftChild(cp(node->leftChild(), treep));
            newNode->rightChild(cp(node->rightChild(), treep));

            if(node == treep)
            {
                this->treep = newNode;
            }
        }
        return newNode;
    }
public:
    BinTree(Node *root = nullptr, Node *treep = nullptr)
    {
        this->root = root;              //konstruktor de cpp
        this->treep = treep;
    }
    ~BinTree()
    {
        deltree(root);
    }
    BinTree(const BinTree & old)
    {
        root = cp(old.root, old.treep);     //copy konstruktor
    }
    BinTree & operator=(const BinTree & old)        //copy override
    {
        BinTree tmp{old};
        std::swap(*this, tmp);
        return *this;
    }
    BinTree(BinTree && old)
    {
        root = nullptr;
        *this = std::move(old);
    }
    BinTree & operator=(BinTree && old)
    {
        std::swap(old.root, root);
        std::swap(old.treep, treep);
        return *this;
    }
    BinTree & operator<<(T value);  //operator override
    void print() { print(root, std::cout); }
    void print(Node * node, std::ostream & os);
    void deltree(Node * node);

};
template <typename T>
class LZWTree : public BinTree<T>
{
public:
    LZWTree() : BinTree<T>(new typename BinTree<T>::Node('/'))
    {
        this->treep = this->root;
    }
    LZWTree & operator<<(T value);
};
template <typename T>
BinTree<T> & BinTree<T>::operator<<(T value)        //overridoljuk az << operátort hogy illesszen be elemeket
{
    if (!treep)
    {
        root = treep = new Node(value);
    }
    else if (treep->getValue() == value)
    {
        treep->incCount();
    }
    else if (treep->getValue() > value)
    {
        if (!treep->leftChild())
        {
            treep->leftChild(new Node(value));
        }
        else
        {
            treep = treep->leftChild();
            *this << value;
        }

    }
    else if (treep->getValue() < value) {
        if (!treep->rightChild())
        {
            treep->rightChild(new Node(value));
        }
        else
        {
            treep = treep->rightChild();
            *this << value;
        }

    }

    treep = root;

    return *this;
}
template <typename T>
LZWTree<T> & LZWTree<T>::operator<<(T value)        //operátor override de most LZW specifikus
{

    if (value == '0')
    {
        if (!this->treep->leftChild())
        {
            typename BinTree<T>::Node * node = new typename BinTree<T>::Node(value);
            this->treep->leftChild(node);
            this->treep = this->root;
        }
        else
        {
            this->treep = this->treep->leftChild();
        }

    }
    else
    {
        if (!this->treep->rightChild())
        {
            typename BinTree<T>::Node * node = new typename BinTree<T>::Node(value);
            this->treep->rightChild(node);
            this->treep = this->root;
        }
        else
        {
            this->treep = this->treep->rightChild();
        }

    }

    return *this;
}
template <typename T>
void BinTree<T>::print(Node *node, std::ostream & os)       //kiprinteli a fát
{
    if (node)
    {
        depth++;
        print(node->leftChild(), os);

        for (int i{ 0 }; i < depth; i++) {
            os << "---";
        }
        os << node->getValue() << " " << depth << " " << node->getCount() << std::endl;
        print(node->rightChild(), os);
        depth--;
    }
}
template <typename T>
void BinTree<T>::deltree(Node *node)
{
    if (node)       //fa posztorder törlése
    {
        deltree(node->leftChild());
        deltree(node->rightChild());

        delete node;
    }
}
int main()
{
    BinTree<int> binTree;
    binTree << 2 << 3 << 1 << 4 << 8;
    binTree.print();
    std::cout << std::endl;
    LZWTree<char> LZWfa;
    LZWfa << '1' << '0' << '1' << '0' << '1' << '1' << '1' << '1' << '0' << '1' << '0' << '1' << '0' << '0' << '0' << '1' << '0' << '0' << '1' << '0';
    LZWfa.print();
    return 0;
}