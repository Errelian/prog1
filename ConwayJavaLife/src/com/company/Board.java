package com.company;
class Board
{
    public int[][] cellneighbour = new int[103][103];   //minden cella két tulajdonsággal rendelkezik: hogy él-e, és hogy hány elő szomszédja van
    public boolean[][] cell = new boolean[103][103];
    public void startup()
    {
        for (int i = 2; i<100;i++)
        {
            for(int j = 2; j < 100; j++)
            {
                    cell[i][j] = false;
                    cellneighbour[i][j]= 0; //inicializálunk mindent a biztonság kedvéért
            }
        }
    }
    public void nextgen()
    {
        for(int i = 2; i <100;i++)
        {
            for(int j =2; j <100; j++)  //2-től 100ig megyek csak hogy ne kelljen ellenőrizni hogy egy szomszéd létezik-e
            {
                if(cell[i-1][j-1]){cellneighbour[i][j]++;}   //megszámoljuk adott cella szomszédait, csúnya de így nem dob Nullptr exceptiont
                if(cell[i-1][j]){cellneighbour[i][j]++;}
                if(cell[i-1][j+1]){cellneighbour[i][j]++;}
                if(cell[i][j-1]){cellneighbour[i][j]++;}
                if(cell[i][j+1]){cellneighbour[i][j]++;}
                if(cell[i+1][j-1]){cellneighbour[i][j]++;}
                if(cell[i+1][j]){cellneighbour[i][j]++;}
                if(cell[i+1][j+1]){cellneighbour[i][j]++;}
            }
        }
        for (int i =2; i <100; i++)
        {
            for (int  j=2; j<100;j++)
            {
                if(cellneighbour[i][j] > 3 || cellneighbour[i][j] < 2) //szomszédok számolása és a sejtstátusz frissítése
                {
                    cell[i][j] = false;
                }
                else
                {
                    if (cellneighbour[i][j] == 3)
                    {
                        cell[i][j] = true;
                    }
                }
                cellneighbour[i][j] = 0;
            }
        }
    }
}
