package com.company;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        JFrame window = new JFrame();  //nyitunk egy ablakot
        window.setTitle("Conway's Game of Life");       //ablak neve
        window.getContentPane().setLayout(new FlowLayout());    //hogy elhejezhessük a képet benne
        Board cellBoard = new Board();  //új cella tábla
        cellBoard.startup();    //inicializáljuk
        while(true)
        {
            cellBoard.nextgen();
            BufferedImage image = new BufferedImage(103, 103, BufferedImage.TYPE_4BYTE_ABGR);       //új kép objektum létrehozása
            for (int i=2; i<100; i++) {
                for(int j=2; j<100; j++) {
                    if (cellBoard.cell[i][j]) {         //ha adott cella él akkor fekete pixel
                        image.setRGB(i, j, 255);
                    }
                    else
                    {                                   //ha nem akkor fehér
                        image.setRGB(i, j, 0);
                    }
                }
            }
            window.getContentPane().add(new JLabel(new ImageIcon(image)));
            window.pack();
            window.setVisible(true);
            Thread.sleep(1000);
        }
    }
}
