experiment_number=10000000     #How many times do we do the experiment?
prize = sample(1:3, experiment_number, replace=T) #We always generate where the prize is
player = sample(1:3, experiment_number, replace=T) #Which door does the player choose first?
MP=vector(length = experiment_number) #Which one will Monty Hall choose?
# sample() - takes a sample, either with or without replacement
#Case1: We don't change the door we choose
for (i in 1:experiment_number) {

  if(prize[i]==player[i]){

    origin=setdiff(c(1,2,3), prize[i])

  }else{

    origin=setdiff(c(1,2,3), c(prize[i], player[i]))

  }

  MP[i] = origin[sample(1:length(origin),1)]

}

non_change_wins= which(prize==player) #non-change wins, wins=true
change=vector(length = experiment_number)
#case 2: We change from the door we had originally chosen
for (i in 1:experiment_number) {

  holvalt = setdiff(c(1,2,3), c(MP[i], player[i]))
  change[i] = holvalt[sample(1:length(holvalt),1)]

}

change_wins = which(prize==change)


sprintf("Number of experiments: %i", experiment_number)
print("How many times do we win by not changing?:", quote = FALSE)
print(length(non_change_wins))
print("How many times do we win by changing?:",quote=FALSE)
print(length(change_wins))
print("Change/Non-Change",quote=FALSE)
print(length(non_change_wins)/length(change_wins))
print("Change+Change", quote=FALSE)
print(length(non_change_wins)+length(change_wins))
