#include <iostream>
#include <iomanip>                                          //a feladatot eredetileg BevProgra csináltam meg még c++ban, ez a githubos változat rehostja, eredetileg host4olt fájl: https://github.com/Errelian/PROG0/blob/master/PRCPP.cpp
#include <cmath>                                            //By: Görög Balázs
#include <vector>
#include <algorithm>
using namespace std;
int main() {
    double linkmatrix[4][4] = {
            {0.0,0.0,1.0/3,0.0},
            {1.0,1.0/2.0,1.0/3.0,1.0},                         //egy 4x4-es double mátrix inicializálása
            {0.0,1.0/2.0,0.0,0.0},
            {0.0,0.0,1.0/3.0,0.0}
    };
    double elozo[4] = {1.0/4.0,1.0/4.0,1.0/4.0,1.0/4.0};    //az elozo iteracio soranti ertek,1/4-el kezd
    double ujabb[4] = {0.0,0.0,0.0,0.0};                    //az ujabb iteracio soran szerszett ertek
    // double teszt;                                        // nem használt teszt változó
    for (int i=0;i<10;i++){                                  //i= az iteráció sorszáma-1
        for (int j=0; j<4; j++){                            //j= a link célpontja, a bementtel rendelkező oldal
            for (int a=0; a<4; a++){                        //a= a link tulajdonosa, a kimettel rendelkező oldal
                if (a==0){ujabb[j] = 0.0;}                  //ha újonnan kezdi az oldal kiírtékelését akkor reseteli a pagerank értéket az újabban, nem elegáns de jó
                if (linkmatrix[j][a] !=0.0) {               //nem végez fölösleges aritmetikai műveletet ha 0 az érték, vagyis nincs link, (az irányított gráfnak ilyne éle nincs)
                    ujabb[j] = ujabb[j]+(elozo[a]*linkmatrix[j][a]);       //elosztja az elozo linktulajdonos PR értékét a kimenő linkjei számával és hozzáadja a tobbi kimenet által kappothoz
                }
            }
        }
        for (int l=0;l<4;l++){elozo[l]=ujabb[l];}               //atírja az új értéket a régibe mielőtt új iterációt kezd
        //teszt = ujabb[1]+ujabb[0]+ujabb[2]+ujabb[3];          //ideiglenes teszt algoritmus, nem működött és nem volt energiám jót írni
        //if (teszt !=1.0){cout << "Szum = " << teszt <<" Valami nem jó";}
        for (int l=0; l<4;l++){cout << ujabb[l] <<'\n'; if (l==3){cout << "-------------------------------\n";}} //kiírja az iterácó értékét és elválasztj  többitől
    }
    return 0;
}