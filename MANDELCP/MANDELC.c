#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#define ITERMAX 25
#define XCORD 64
#define YCORD 64
#define MAXLIMIT 2
#define MINLIMIT -2
int mappingSquare(float Zreal, float Zimaginary, float Creal, float Cimaginary, float* realDest, float* imaginaryDest)
{

    float temp = Zreal * Zreal - Zimaginary * Zimaginary + Creal;
    *imaginaryDest = 2.0f * Zreal * Zimaginary + Cimaginary;
    *realDest = temp;
    return 0;
}
int MandelCalc(float real, float imaginary)
{
    float Zreal = 0;
    float Zimaginary = 0;
    int i = 0;
    while(i <ITERMAX && fabs(Zreal)<2.0f && fabs(Zimaginary)<2.0f)
    {
        mappingSquare(Zreal, Zimaginary, real, imaginary, &Zreal, &Zimaginary );
        i++;
    }
    return i;
}
void redraw()
{
    char** MandelArray = (char**) malloc(XCORD * sizeof(char**));
    for (int i=0; i <YCORD; i++)
    {
        MandelArray[i] = (char*) malloc(YCORD * sizeof(char*));
    }
    for (int i=0; i <XCORD; i++)
    {
        for (int j = 0; j <YCORD; j++)
        {
            float TempX = (float)i / (float)XCORD * ((float)MAXLIMIT - (float)MINLIMIT) + (float)MINLIMIT;
            float TempY = (float)j / (float)YCORD * ((float)MAXLIMIT - (float)MINLIMIT) + (float)MINLIMIT;
            float CurrentMvalue = MandelCalc(TempX,TempY);
            if(CurrentMvalue < 19.0f){printf( "i");} else {printf("X");}
        }
        printf("\n");
    }
    for (int i=0; i <YCORD; i++)
    {
        free(MandelArray[i]);
    }
    free(MandelArray);
}
int main() {
    redraw();
    return 0;
}