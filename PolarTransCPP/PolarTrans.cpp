#include <PolarTrans.h>

double PolarGeneration::GetNext()
{
	if (!stored)
	{
		double u1, u2, v1, v2, w;
		do
		{
			u1 = std::rand() / (RAND_MAX + 1.0);
			u2 = std::rand() / (RAND_MAX + 1.0);
			v1 = 2 * u1 - 1;
			v2 = 2 * u2 - 1;
			w = v1 * v1 + v2 * v2;
		} while (w > 1);
		double r = std::sqrt((-2 * std::log(w)) / w);
		current = r * v2;
		stored = true;
		return r * v1;
	}
	else
	{
		stored = false;
		return Current;
	}
}