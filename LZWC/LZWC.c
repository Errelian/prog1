#include <stdio.h>
#include <stdlib.h>
typedef struct fa fa;
struct fa
{
    fa* left;
    fa* right;
    int value;
};
int insert_fa(fa* target, const char* toInsert, int length)
{
    fa* curr = target;
    int strIndex = 0;
    int success = 0;
    while(!success && (strIndex < length))
    {
        if(toInsert[strIndex] == '0')
        {
            if(curr -> left)
            {
                curr = curr -> left;
                strIndex++;
            }
            else
            {
                curr -> left = (fa*) malloc(sizeof(fa));
                curr -> left -> left = 0;
                curr -> left -> right = 0;              //itt lehet át kéne léptetni a curr = curr -> left el de idk
                curr -> left -> value = 0;
                strIndex++;
                success = 1;
            }
        }
        else {
            if (curr->right) {
                curr = curr->right;
                strIndex++;
            } else {
                curr->right = (fa *) malloc(sizeof(fa));
                curr->right->left = 0;
                curr->right->right = 0;              //itt lehet át kéne léptetni a curr = curr -> right al de idk
                curr->right->value = 1;
                strIndex++;
                success = 1;
            }
        }
    }
    return 0;
}
int preorder(fa* curr)
{
    //do things
    if (curr->left)
        preorder(curr->left);
    if(curr->right)
        preorder(curr->right);
    return 0;
}
int postorder(fa* curr)
{
    if (curr->left)
        postorder(curr->left);
    if(curr->right)
        postorder(curr->right);
    //do things
    return 0;
}