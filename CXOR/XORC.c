#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
char* strlwr2(char *str)                         //converts strings to lowercase source: //https://stackoverflow.com/a/23618467/9281022
{
    unsigned char *p = (unsigned char *)str;
    while (*p) {
        *p = tolower((unsigned char)*p);
        p++;
    }
    return str;
}

char* key()  //defines a cypher key and returns it
{
    char* DefKey = "1234";  //default cypher key

    return DefKey;
}

int xor_encrypt(const char* CypherKey, size_t KeyL, const char* text,size_t size, char* container){
    size_t real_size = size-1;

    for (size_t i =0; i < real_size; i++){

        unsigned char textChar = (unsigned char)text[i];

        unsigned char cypherChar = (unsigned char)CypherKey[i%KeyL];
        //gets a certain character of the cypher
        container[i] = (char)(textChar ^ cypherChar);
    }
    container[real_size] = 0;
    return 1;
}

int xor_decrypt(const char* CypherKey, size_t KeyL, char* text, size_t size, char* container){

    return xor_encrypt(CypherKey,KeyL,text, size ,container);

}

unsigned int count_cyphertext[256];

float distribution_cyphertext[256];

int xor_1byte_crack(const char* source, size_t size, char* destination)
{
    size_t i = size;                            //erősen Nagy Viktor Márk kódja által ihletve
    while (i-->0) {
        count_cyphertext[(unsigned char) source[i]]++;   //megszámolja hogy minden karakterből hány van, tudom hogy nagyon csúnya, az explicit csatolást meg tiltani kéne
    }
                                                        //disztribúció megszámolása
    i = 256;
    while(i-->0){
        if (count_cyphertext > 0)
        {
            distribution_cyphertext[i] = (float) count_cyphertext[i] / (float) size;
        }
    }
                                                    //associating frequence with space
    int max_cipher_index = 0;
    for (int k = 0; k < 256; k++)
    {
        if (distribution_cyphertext[k] > distribution_cyphertext[max_cipher_index]) //legtöbb előfordulású keresése
            max_cipher_index = k;
    }
    unsigned char key = (unsigned char)' ' ^ (unsigned char)max_cipher_index;

    i = size;
    while (i-- > 0) {
        destination[i] = (char) ((unsigned char) (source[i]) ^ key);
    }
    return 0;

}

int xor_crack_variable( const char* source, size_t size, char* destination, int keyLength)
{
    char **asd = (char**)malloc(sizeof(char*)*keyLength);

    for (int i = 0; i < keyLength; i++)
    {
        asd[i] = (char*)malloc(sizeof(char)*(size / keyLength) + 1 + 1);
    }

    for (size_t i = 0; i < size; i += keyLength)
    {
        for (int j = 0; j < keyLength; j++)
        {
            asd[j][i / keyLength] = source[i + j];
        }

    }

    for (int i = 0; i < keyLength; i++)
    {
        xor_1byte_crack(asd[i], (size / keyLength) + 2, asd[i]);            //ez csak visszavezeti 1 bytera a sokbytos szöveget
    }

    for (size_t i = 0; i < size; i += keyLength)
    {
        for (int j = 0; j < keyLength; j++)
        {
            destination[i+j] = asd[j][i / keyLength];
        }

    }

    return 0;
}

int Encrypt_Test()
{
    char set_text[] ="What the heck did you just hecking say about me, you little kid? I'll have you know I graduated top of my class in the Navy Seals, and I've been involved in numerous secret raids on Al-Quaeda, and I have over three-hundred confirmed kills. I am trained in gorilla warfare and I'm the top sniper in the entire US armed forces. You are nothing to me but just another target. I will wipe you the heck out with precision the likes of which has never been seen before on this Earth, mark my hecking words. You think you can get away with saying that crap to me over the Internet? Think again, hecker. As we speak I am contacting my secret network of spies across the USA and your IP is being traced right now so you better prepare for the storm, maggot. The storm that wipes out the pathetic little thing you call your life. You're hecking dead, kid. I can be anywhere, anytime, and I can kill you in over seven hundred ways, and that's just with my bare hands. Not only am I extensively trained in unarmed combat, but I have access to the entire arsenal of the United States Marine Corps and I will use it to its full extent to wipe your miserable bum off the face of the continent, you little crap. If only you could have known what unholy retribution your little clever comment was about to bring down upon you, maybe you would have held your hecking tongue. But you couldn't, you didn't, and now you're paying the price, you goddamn idiot. I will crap fury all over you and you will drown in it. You're hecking dead, kiddo.";

    char* key1 = key();

    strlwr2(set_text);

    char* destination1 = (char*) malloc(sizeof(char)*strlen(set_text));

    xor_encrypt(key1,strlen(key1),set_text,strlen(set_text),destination1);

    printf("%s",destination1);

    char* destination2 = (char*) malloc(sizeof(char)*strlen(destination1));

    xor_decrypt(key1,strlen(key1),destination1,strlen(destination1), destination2);

    printf("\n");   //this will somehow cause a memory access violation from time to time

    printf("%s",destination2);

    return 2;
}

int main(){
    Encrypt_Test();
    return 0;
}