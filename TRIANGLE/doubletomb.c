#include <stdio.h>
#include <stdlib.h>

int main() {
    int size = 6;
    double **tm;
    tm = (double **) malloc(size * sizeof(double *));       //lefoglalja az egész tömb helyét
    for (int i = 0; i < size; i++) {
        tm[i] = (double *) malloc((i + 1) *
                                  sizeof(double));             //lefoglalja adott tömbelem helyét ahol értelmezve van (sor+1 helyet foglal le double pointereknek)
    }
    for (int i = 0; i < size; ++i)
        for (int j = 0; j < i + 1; ++j) {
            tm[i][j] = i * (i + 1) / 2 + j;
            printf("%f, ", tm[i][j]);
            printf("\n");           //értékek belírása a dereferált tömbbe és az értékek kiírása a tesztelés kedvéért
        }
    for (int i = 0; i < size; ++i)
        free(tm[i]);                       //adott tömbelemek felszabadítása
    free(tm);                              //tömb felszabadítása
    return 0;
}