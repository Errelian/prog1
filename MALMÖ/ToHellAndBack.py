from __future__ import print_function
from builtins import range
import MalmoPython
import os
import sys
import time
import random
import json
import math

if sys.version_info[0] == 2:
	# flush print output immediately
	sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)
else:
	import functools
	print = functools.partial(print, flush=True)

# Create default Malmo objects:
agent_host = MalmoPython.AgentHost()
try:
	agent_host.parse(sys.argv)
except RuntimeError as e:
	print('ERROR:', e)
	print(agent_host.getUsage())
	exit(1)
if agent_host.receivedArgument("help"):
	print(agent_host.getUsage())
	exit(0)

# -- set up the mission -- #
missionXML_file = 'nb4tf4i_d.xml'
with open(missionXML_file, 'r') as f:
	mission_xml = f.read()
	my_mission = MalmoPython.MissionSpec(mission_xml, True)
	my_mission.drawBlock(0, 0, 0, "lava")


class Steve:
	def __init__(self, agent_host):
		self.agent_host = agent_host

		self.x = 0
		self.y = 0
		self.z = 0
		self.yaw = 0
		self.pitch = 0

		self.start = True

		self.corners = 0

		self.front_of_me_idx = 0
		self.front_of_me_idxr = 0
		self.front_of_me_idxl = 0
		self.right_of_me_idx = 0
		self.left_of_me_idx = 0

		self.nof_red_flower = 0
		self.lookingat = ""
		self.attackLvl = 0

		self.foundLava = 1

		self.collectedFlowers = {}
		for i in range(100):
			self.collectedFlowers[i] = False

		self.collectedFlowers[1] = True
		self.collectedFlowers[2] = True

	def idle(self, delay):
		# print("      SLEEPING for ", delay)
		time.sleep(delay)

	def isInTrap(self, nbr):

		dc = 0
		nbri = [9, 10, 11, 12, 14, 15, 16, 17]
		for i in range(1, len(nbri)):
			if nbr[nbri[i]] == "dirt":
				dc = dc + 1
		return dc > 5

	def calcNbrIndex(self):
		if self.yaw >= 180-22.5 and self.yaw <= 180+22.5:
			self.front_of_me_idx = 1
			self.front_of_me_idxr = 2
			self.front_of_me_idxl = 0
			self.right_of_me_idx = 5
			self.left_of_me_idx = 3
		elif self.yaw >= 180+22.5 and self.yaw <= 270-22.5:
			self.front_of_me_idx = 2
			self.front_of_me_idxr = 5
			self.front_of_me_idxl = 1
			self.right_of_me_idx = 8
			self.left_of_me_idx = 0
		elif self.yaw >= 270-22.5 and self.yaw <= 270+22.5:
			self.front_of_me_idx = 5
			self.front_of_me_idxr = 8
			self.front_of_me_idxl = 2
			self.right_of_me_idx = 7
			self.left_of_me_idx = 1
		elif self.yaw >= 270+22.5 and self.yaw <= 360-22.5:
			self.front_of_me_idx = 8
			self.front_of_me_idxr = 7
			self.front_of_me_idxl = 5
			self.right_of_me_idx = 6
			self.left_of_me_idx = 2
		elif self.yaw >= 360-22.5 or self.yaw <= 0+22.5:
			self.front_of_me_idx = 7
			self.front_of_me_idxr = 6
			self.front_of_me_idxl = 8
			self.right_of_me_idx = 3
			self.left_of_me_idx = 5
		elif self.yaw >= 0+22.5 and self.yaw <= 90-22.5:
			self.front_of_me_idx = 6
			self.front_of_me_idxr = 3
			self.front_of_me_idxl = 7
			self.right_of_me_idx = 0
			self.left_of_me_idx = 8
		elif self.yaw >= 90-22.5 and self.yaw <= 90+22.5:
			self.front_of_me_idx = 3
			self.front_of_me_idxr = 0
			self.front_of_me_idxl = 6
			self.right_of_me_idx = 1
			self.left_of_me_idx = 7
		elif self.yaw >= 90+22.5 and self.yaw <= 180-22.5:
			self.front_of_me_idx = 0
			self.front_of_me_idxr = 1
			self.front_of_me_idxl = 3
			self.right_of_me_idx = 2
			self.left_of_me_idx = 6
	def run(self):
		world_state = self.agent_host.getWorldState()

		self.agent_host.sendCommand("look 1")
		self.agent_host.sendCommand("look 1")
		self.agent_host.sendCommand("turn -1")
		self.agent_host.sendCommand("turn -1")

		# Loop until mission ends:
		while world_state.is_mission_running:

			act = self.action(world_state)
			if not act:
				self.idle(.017)

			world_state = self.agent_host.getWorldState()

	def StepUp(self, nbrs):
		turns = 0

		if nbrs[14] == "dirt":
			pass
		elif nbrs[16] == "dirt":
			turns = 1
		elif nbrs[12] == "dirt":
			turns = 2
		elif nbrs[10] == "dirt":
			turns = 3

		for i in range(turns):
			self.agent_host.sendCommand("turn 1")
			time.sleep(.2)

		self.agent_host.sendCommand("jumpmove 1")
		time.sleep(.2)

		for i in range(turns):
			self.agent_host.sendCommand("turn -1")
			time.sleep(.2)

	def action(self, world_state):
		for error in world_state.errors:
			print("Error:", error.text)

		if world_state.number_of_observations_since_last_state == 0:
			return False

		input = world_state.observations[-1].text
		observations = json.loads(input)
		nbr = observations.get("nbr3x3", 0)

		# update location
		self.x = observations["XPos"]
		self.y = observations["YPos"]
		self.z = observations["ZPos"]

		# turn if lava
		if self.foundLava == 1 and "lava" in nbr: #(nbr[14] == "lava" or nbr[23] == "lava") :
			print(nbr[14])
			self.foundLava = -1
			print("FOUND LAVA")
			return

		# escape from lava
		if self.foundLava == -1:
			print(nbr[14])
			self.agent_host.sendCommand("move "+str(self.foundLava))
			time.sleep(.1)
			print("escaping lava")
			return

		# go to edge
		if nbr[14] == "air" or nbr[14] == "red_flower":
			print(nbr[14])
			self.agent_host.sendCommand("move "+str(self.foundLava))
			time.sleep(.4)
			print("going to ledge")
			return

		# advance level
		if nbr[14] == "dirt":
			print(nbr[14])
			self.agent_host.sendCommand("jumpmove "+str(self.foundLava))
			time.sleep(.4)
			print("climbing ledge")
			return

		return
# try to start mission
num_repeats = 1
for ii in range(num_repeats):

	my_mission_record = MalmoPython.MissionRecordSpec()

	# Attempt to start a mission:
	max_retries = 6
	for retry in range(max_retries):
		try:
			agent_host.startMission(my_mission, my_mission_record)
			break
		except RuntimeError as e:
			if retry == max_retries - 1:
				print("Error starting mission:", e)
				exit(1)
			else:
				print("Attempting to start the mission:")
				time.sleep(2)

	# Loop until mission starts:
	print("   Waiting for the mission to start")
	world_state = agent_host.getWorldState()

	while not world_state.has_mission_begun:
		time.sleep(0.15)
		world_state = agent_host.getWorldState()
		for error in world_state.errors:
			print("Error:", error.text)

	print("NB4tf4i Red Flower Hell running\n")
	steve = Steve(agent_host)
	steve.run()
	print("Number of flowers: " + str(steve.nof_red_flower))
	time.sleep(3)

print("Mission ended")
# Mission has ended.
