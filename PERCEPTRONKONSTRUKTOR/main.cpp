#ifndef QL_HPP
#define QL_HPP


#include <iostream>
#include <cstdarg>
#include <map>
#include <iterator>
#include <cmath>
#include <random>
#include <limits>
#include <fstream>

#include "nlp.hpp"
#include "qlc.h"

#ifndef Q_LOOKUP_TABLE
class Perceptron
{
public:
    Perceptron(int nof, ...) // a ...-nak köszönhetően akárhány argumentuma lehet a konstruktornak
    {
        n_layers = nof;
        //rétegszám
        units = new double*[n_layers];
        //a rétegekre mutató pointerek, vagyis a rétegek száma
        n_units = new int[n_layers];
        //a rétegek értéke
        va_list vap;
        //a véltozó méretű argomentumlista értelmezéséhez szükséges változó és annak az inicializálása
        va_start(vap, nof);

        for (int i{ 0 }; i < n_layers; ++i)
        {
            n_units[i] = va_arg(vap, int);

            if (i)	//minden nem első iterációban lefut
                units[i] = new double[n_units[i]];	//rétegméret beállítása
        }

        va_end(vap);
        // a parsoló változó törlése
        weights = new double**[n_layers - 1];
        //előzetes rétegben lévő neuronák által ráhatott súlya
#ifndef RND_DEBUG
        std::random_device init;
        std::default_random_engine gen{ init() };
#else
        std::default_random_engine gen;
#endif

        std::uniform_real_distribution<double> dist(-1.0, 1.0);		// a súlyok randomizálása, azt hiszem

        for (int i{ 1 }; i < n_layers; ++i)	//minden layerre lefut, kivéve az elsőre, mert azokra vonatkozóan nincs összeköttetés
        {
            weights[i - 1] = new double *[n_units[i]];

            for (int j{ 0 }; j < n_units[i]; ++j)	//annyiszor fut, ahány neuron van a mostani layerben
            {
                weights[i - 1][j] = new double[n_units[i - 1]]; //double pointer tömböket töltünk meg double tömbökkel, annyiszor ahány neuron volt az előző rétegben

                for (int k{ 0 }; k < n_units[i - 1]; ++k)	//az előző réteg neuronszámaszor fut le
                {
                    weights[i - 1][j][k] = dist(gen);	//véletlen súly settelése
                }
            }
        }
    }

    Perceptron(std::fstream & file)
    {
        file >> n_layers;

        units = new double*[n_layers];
        n_units = new int[n_layers];

        for (int i{ 0 }; i < n_layers; ++i)
        {
            file >> n_units[i];

            if (i)
                units[i] = new double[n_units[i]];
        }

        weights = new double**[n_layers - 1];

        for (int i{ 1 }; i < n_layers; ++i)
        {
            weights[i - 1] = new double *[n_units[i]];

            for (int j{ 0 }; j < n_units[i]; ++j)
            {
                weights[i - 1][j] = new double[n_units[i - 1]];

                for (int k{ 0 }; k < n_units[i - 1]; ++k)
                {
                    file >> weights[i - 1][j][k];
                }
            }
        }
    }
#endif
