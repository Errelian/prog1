
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>

__global__ void mappingSquare(int* Destination, float* cReal, float* cImag)
{
    int index = threadIdx.x + (blockIdx.x * blockDim.x);
    int MandelValue = 0;
    float zRealLoc = cReal[index];  
    float zImagLoc = cImag[index];
    for (MandelValue = 0; MandelValue < 64; MandelValue++)                              //calculates the MandelValue of each pixel up to a depth of 64
    {
        float temp = zRealLoc * zRealLoc - zImagLoc * zImagLoc + cReal[index];
        zImagLoc = 2.0f * zRealLoc* zImagLoc + cImag[index];
        zRealLoc = temp;
        if (fabs(zRealLoc) > 2.0 || fabs(zImagLoc) > 2.0)
            break;
    }
    Destination[index] = MandelValue;
}
void MandelCuda(unsigned short int* iter, const float* bR, const float* bI, unsigned int WIDTH, unsigned int HEIGHT)
{
    unsigned int size = WIDTH * HEIGHT;
    float* dev_bR = 0;
    float* dev_bI = 0;
    unsigned short int* dev_iter = 0;
    cudaError asd;
    cudaSetDevice(0);
    asd = cudaMalloc((void**)&dev_iter, size * sizeof(unsigned short));
    asd = cudaMalloc((void**)&dev_bR, size * sizeof(float));
    asd = cudaMalloc((void**)&dev_bI, size * sizeof(float));

    asd = cudaMemcpy(dev_bR, bR, size * sizeof(float), cudaMemcpyHostToDevice);
    asd = cudaMemcpy(dev_bI, bI, size * sizeof(float), cudaMemcpyHostToDevice);

    dim3 dimBlock = dim3(std::min(1024u, size));                                       
    dim3 dimGrid = dim3(std::ceil((float)size / 1024));                                 //i have absolutely no idea why this does what it does

    mappingSquare<<<dimGrid, dimBlock>>>(dev_iter, dev_bR, dev_bI);                     //i have absolutely  no idea why this doesnt build
    cudaDeviceSynchronize();
    cudaMemcpy(iter, dev_iter, size * sizeof(unsigned short), cudaMemcpyDeviceToHost);
}
int main() 
{
    const unsigned int WIDTH = 512;
    const unsigned int HEIGHT = 512;
    float* bReal = new float[WIDTH * HEIGHT];
    float* bImag = new float[WIDTH * HEIGHT];
    unsigned short int* iter = new unsigned short int[WIDTH * HEIGHT];

    for (int i = 0; i < WIDTH; i++)
        for (int j = 0; j < HEIGHT; j++)
        {
            bReal[i * HEIGHT + j] = (float)i / WIDTH * 4.0 - 2.0;
            bImag[i * HEIGHT + j] = (float)j / HEIGHT * 4.0 - 2.0;
        }
    MandelCuda(iter, bReal, bImag, WIDTH, HEIGHT);
    cudaDeviceReset();
    delete[] bReal;
    delete[] bImag;

    std::ofstream out;
    out.open("out.pgm");

    out << "P2\n";
    out << HEIGHT << " " << WIDTH << "\n";
    out << "63\n";

    for (int i = 0; i < WIDTH; i++)
        for (int j = 0; j < HEIGHT; j++, out << "\n")
            out << iter[i * HEIGHT + j] << " ";

    out.close();
    delete[] iter;
}
