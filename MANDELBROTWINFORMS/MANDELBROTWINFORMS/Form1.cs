﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;
namespace MANDELBROTWINFORMS
{ //kódot inspiráló kód: https://rosettacode.org/wiki/Mandelbrot_set#C.23
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            pictureBox1.MouseWheel += new MouseEventHandler(zoomHandler);
            pictureBox1.MouseDown += new MouseEventHandler(delegate (object sender, MouseEventArgs e)
            {
                panningStart = e.Location;
                MaxPanLimitR = MaxLimitR;
                MinPanLimitR = MinLimitR;
                MaxPanLimitI = MaxLimitI;
                MinPanLimitI = MinLimitI;
                Panning = true;
            });
            pictureBox1.MouseUp += new MouseEventHandler(delegate (object sender, MouseEventArgs e) { Panning = false; Mdraw(); });
            pictureBox1.MouseMove += new MouseEventHandler(PanningHandler);
        }
        private float MaxLimitR = 2;
        private float MinLimitR = -2;
        private float MaxLimitI = 2;           //declaring the default limits
        private float MinLimitI = -2;
        private short itermax = 100;
        public byte MandelBrot(Complex F)
        {
            Complex F_ = F;
            byte i = 0;
            for (i = 0; i < itermax; i++)
            {
                F_ = F_ * F_ + F;
                if (F_.Magnitude >= 2)
                    break;
            }
            return i;
        }
        public Complex MandelCalc(Complex Z, Complex C)
        {
            return Z * Z + C;
        }
        public int BioMorph(Complex F)
        {
            Complex F_ = F;
            Complex D = new Complex(-0.8, 0.156);
            Complex V = new Complex(0.0, 0.0);
            int i = 0;
            for (i = 0; i < itermax; i++)
            {
                V = MandelCalc(F_, D);
                F_ = MandelCalc(V, D);
                i++;

                if (F_.Magnitude > 2)
                    break;
            }
            return i;
        }
        Bitmap MandelBitmap;
        void Mdraw()
        {
            ushort width = 1920;
            ushort height = 1080;
            MandelBitmap = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            for (ushort i = 0; i < width; i++)
            {
                for (ushort j = 0; j < height; j++)
                {
                    int CurrentMvalue = BioMorph(new Complex(
                                  (float)i / width * (MaxLimitR - MinLimitR) + MinLimitR,
                                  (float)j / height * (MaxLimitI - MinLimitI) + MinLimitI));    
                    Color asd = Color.FromArgb(CurrentMvalue/2, CurrentMvalue*2, CurrentMvalue*2);
                    MandelBitmap.SetPixel(i, j, asd);
                }
            }
            pictureBox1.Image = MandelBitmap;
        }
        public float ZoomingScaleX = 0.002f;
        public float ZoomingScaleY = 0.002f;
        void zoomHandler(object sender, MouseEventArgs f)
        {
            float ZoomAmountR = (MaxLimitR - MinLimitR) * f.Delta * ZoomingScaleX;
            float ZoomAmountI = (MaxLimitI - MinLimitI) * f.Delta * ZoomingScaleY;

            MaxLimitR = MaxLimitR - ZoomAmountR / 2;
            MaxLimitI = MaxLimitI - ZoomAmountI / 2;
            MinLimitR = MinLimitR + ZoomAmountR / 2;
            MinLimitI = MinLimitI + ZoomAmountI / 2;
            Mdraw();
        }
        float PanScaleX = 0.005f;
        float PanScaleY = 0.005f;
        bool Panning = false;
        float MaxPanLimitR;
        float MinPanLimitR;
        float MaxPanLimitI;
        float MinPanLimitI;
        Point panningStart;
        void PanningHandler(object sender, MouseEventArgs f)
        {
            if (Panning)
            {
                    MinLimitR = (panningStart.X - f.X) * PanScaleX * (MaxPanLimitR - MinPanLimitR) + MinPanLimitR;
                    MaxLimitR = (panningStart.X - f.X) * PanScaleX * (MaxPanLimitR - MinPanLimitR) + MaxPanLimitR;

                    MinLimitI = (panningStart.Y - f.Y) * PanScaleY * (MaxPanLimitI - MinPanLimitI) + MinPanLimitI;
                    MaxLimitI = (panningStart.Y - f.Y) * PanScaleY * (MaxPanLimitI - MinPanLimitI) + MaxPanLimitI;
                Mdraw();
            }

        }
        void ImageSave()
        {
            MandelBitmap.Save("julia.png", ImageFormat.Png);
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            Mdraw();
            ImageSave();
        }
    }
}
