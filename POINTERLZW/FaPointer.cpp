#include <iostream>
#include <cmath>
#include <fstream>

class LZWBinFa
{
public:

    LZWBinFa ()
    {
        gyoker = new Csomopont();       //megmondjuk a gyökér pointernek hogy mutasson egy újonnan létrehozott Csomópontra
        fa=gyoker;                      //a fában mutató pointer mutasson a gyökérre
    }
    ~LZWBinFa ()
    {
        szabadit (gyoker->egyesGyermek ()); //destruktor, posztorder módon memóriát szabadít fel
        szabadit (gyoker->nullasGyermek ());
        delete gyoker;
    }


    void operator<< (char b)
    {

        if (b == '0')
        {

            if (!fa->nullasGyermek ())
            {
                Csomopont *uj = new Csomopont ('0');    //az épitési logika változatlan az előző változat óta
                fa->ujNullasGyermek (uj);
                fa = gyoker;
            }
            else
            {

                fa = fa->nullasGyermek ();
            }
        }
        else
        {
            if (!fa->egyesGyermek ())
            {
                Csomopont *uj = new Csomopont ('1');
                fa->ujEgyesGyermek (uj);
                fa = gyoker;
            }
            else
            {
                fa = fa->egyesGyermek ();
            }
        }
    }

    void kiir (void)
    {

        melyseg = 0;
        kiir (gyoker, std::cout);
    }

    friend std::ostream & operator<< (std::ostream & os, LZWBinFa & bf)
    {
        bf.kiir (os);       //operátor overload
        return os;
    }
    void kiir (std::ostream & os)
    {
        melyseg = 0;
        kiir (gyoker, os);  //rekurzív kiírás
    }

private:
    class Csomopont
    {
    public:

        Csomopont (char b = '/'):betu (b), balNulla (0), jobbEgy (0) //csomópont konstruktor, ugyan az mint az előző
        {
        };
        ~Csomopont ()
        {
        };

        Csomopont *nullasGyermek () const
        {
            return balNulla;
        }

        Csomopont *egyesGyermek () const //getterek és setterek
        {
            return jobbEgy;
        }

        void ujNullasGyermek (Csomopont * gy)
        {
            balNulla = gy;
        }

        void ujEgyesGyermek (Csomopont * gy)
        {
            jobbEgy = gy;
        }

        char getBetu () const
        {
            return betu;
        }

    private:

        char betu;
        Csomopont *balNulla;
        Csomopont *jobbEgy;
        Csomopont (const Csomopont &) = delete;
        Csomopont & operator= (const Csomopont &) = delete; //hogy ne lehessen meghívni
    };

    Csomopont *fa;
    int melyseg;
    LZWBinFa (const LZWBinFa &) = delete;
    LZWBinFa & operator= (const LZWBinFa &) = delete;   //hogy ne lehessen meghívni

    void kiir (Csomopont * elem, std::ostream & os)
    {

        if (elem != NULL)
        {
            ++melyseg;
            kiir (elem->nullasGyermek (), os);
            for (int i = 0; i < melyseg; ++i)
                os << "---";
            os << elem->getBetu () << "(" << melyseg - 1 << ")" << std::endl;
            kiir (elem->egyesGyermek (), os);
            --melyseg;
        }
    }
    void szabadit (Csomopont * elem)
    {
        if (elem != NULL)
        {
            szabadit (elem->egyesGyermek ());
            szabadit (elem->nullasGyermek ());  //a destruktorban szereplő memóriafelszabadító függvény
            delete elem;
        }
    }

protected:
    Csomopont *gyoker;  //protected arra az esetre ha bővíteni szeretnénk a programot
};

int main ()
{

}
