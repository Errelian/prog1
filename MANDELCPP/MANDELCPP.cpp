#include <iostream>
#include <cmath>
#include <complex>
#include <string>
const int ITERMAX = 30;
const int VALUEMAX = 2;
const int VALUEMIN = -2;
const int XLIMIT = 64;
const int YLIMIT = 64;
const double size = 4.0;
std::complex<double> MandelCalc(std::complex<double> Z, std::complex<double> C)
{
    return Z * Z + C;
}
int biom(std::complex<double> Z_)
{
    std::complex<double> Z(Z_.real(), Z_.imag());
    std::complex<double> D(-0.8, 0.156);
    std::complex<double> V(0, 0);
    int i = 0;
    for(i = 0;i<ITERMAX;i++)
    {
        V = MandelCalc(Z, D);
        Z = MandelCalc(V, D);
        i++;

        if (abs(Z) > VALUEMAX)
            break;
    }

    return i;
}
int Mandelbrot(std::complex<double> Z)
{
    std::complex<double> Z_(0, 0);
    std::complex<double> C(Z.real(), Z.imag());
    int i = 0;
    while (i < ITERMAX && std::abs(Z_.real()) < 2 && std::abs(Z_.imag()) < 2)
    {
        Z_ = MandelCalc(Z_, C);
        i++;
    }
    return i;
}
double xtoreal(int x)
{
    double RealPart;
    RealPart = (double)x / (double)XLIMIT * size + (double)VALUEMIN;
    return RealPart;
}
double ytoimaginary(int y)
{
    double Imaginarypart;
    Imaginarypart = (double)y / (double)YLIMIT * size + (double)VALUEMIN;
    return Imaginarypart;
}
void Mdraw()
{
    for (int i=0; i <XLIMIT; i++)
    {
        for (int j = 0; j <YLIMIT; j++)
        {
            std::complex<double> F( xtoreal(i), ytoimaginary(j));
            int CurrentMvalue = biom(F);
            if (CurrentMvalue < 15) {std::cout << "i";} else {std::cout <<"X";}
        }
        std::cout << "\n";
    }
}

int main() {
    Mdraw();
    return 0;
}